global.confPath = process.argv.length > 2?process.argv[2]:__dirname + "/config.json";
const config = require("./servicios/Config");

async function createHTTPServer() {
  const zServer = require("./servicios/z-server");
  const express = require('express');
  const app = express();
  const bodyParser = require('body-parser');
  const http = require('http');
  const conf = config.getConfig();
  
  // PENDIENTE A INTEGRAR
  const seguridad = require("./services/Seguridad");
  const servicios = require("./services/Servicios");
  const perfiles = require("./services/Perfiles");

  zServer.registerModule("seg", seguridad);
  zServer.registerModule("ser", servicios);
  zServer.registerModule("per", perfiles);

  app.use("/", express.static(__dirname + "/www"));

  app.use(bodyParser.urlencoded({extended:true}));
  app.use(bodyParser.json({limit:"50mb"}));
  app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
      next();
  });

    
  app.post("/*.*", (req, res) => zServer.resolve(req, res));

  if (conf.httpServer) {
    var port = conf.httpServer.port;
    httpServer = http.createServer(app);
    httpServer.listen(port, function () {
      console.log("[Conciliacion] HTTP Server started on port " + port);
    });
  }
}

//  PENDIENTE A INTEGRAR
require('node-cleanup')((exitCode, signal) => {
  console.log("Stopping [TLM] HTTP Server ...", exitCode, signal);
  if (httpServer) httpServer.close();
});

createHTTPServer()
  .then(() =>  console.log("[Conciliacion] Is running"))
  .catch(error => console.log("[Conciliacion] Cannot start", error));