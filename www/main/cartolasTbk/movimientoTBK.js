class movimientoTBK extends ZCustomController {
    async onThis_init() {
        this.lblSubTituloPagina.text = "";
        this.refrescaLocales();
        this.refresca();
        $('.datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            format: 'DD/MM/YYYY'
        });
    }

    refresca() {

    }

    onCmdBuscarTbk_click() {
        this.refrescaLista()
    }

    refrescaLista() {
        let local = this.edLocales.val;
        let fechaVal = this.edFecha.val;
        if (!local || !fechaVal) return;
        this.lista.refresh();
    }

    async refrescaLocales() {
        let urlOz = await zPost("getOzService.seg");
        let urlServicio = urlOz.ozConciliacion + "/getDataServer";
        let params = [];
        let locales = await zPost(urlServicio, { metodo: "getLocales", params: params });
        locales.splice(0, 0, { codigo: null, nombre: "[Seleccionar]" });
        this.edLocales.setRows(locales);
    }


    async onLista_getRows(cb) {
        let urlOz = await zPost("getOzService.seg");
        let urlServicio = urlOz.ozConciliacion + "/getDataServer";
        let local =  this.edLocales.val;
        let fechaVal = this.edFecha.val;
        let fecha = await new moment(fechaVal);
        let params = {
            local: local,
            fecha: fecha
        }

        await zPost(urlServicio, { metodo: "getCartolaDia", params: params }, dataset => {
            cb(dataset);
        });
    }
}
ZVC.export(movimientoTBK);