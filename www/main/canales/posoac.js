class posoac extends ZCustomController {
    onThis_init() {
        this.lblSubTituloPagina.text = "";
        this.refresca();
        $('.datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            format: 'DD/MM/YYYY'
        });
    }
    refresca() {

    }

    onCmdBuscarTbk_click() {
        this.refrescaLista()
    }

    refrescaLista() {
        let fechaVal = "01/12/2020";//this.edFecha.val;
        if (!fechaVal) return;
        this.listaOAC.refresh();
    }

    async onListaOAC_getRows() {
        let urlOz = await zPost("getOzService.seg");
        let urlServicio = urlOz.ozConciliacion + "/getDataServer";
        let fechaVal = this.edFecha.val;
        let fecha = await new moment(fechaVal);
        let params = {
            fecha: fecha
        }

        let dataset = await zPost(urlServicio, { metodo: "getRecaudacionesPosOACSP", params: params });
        return dataset.map(r => (this.prepareRow(r)));
    }
    prepareRow(row){
        return row;
    }
}
ZVC.export(posoac);