class posverifone extends ZCustomController{
    onThis_init() {
        this.lblSubTituloPagina.text = "";
        this.refresca();
        $('.datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },
            format: 'DD/MM/YYYY'
        });
    }
    refresca() {
    }

    onCmdBuscarTbk_click() {
        this.refrescaLista()
    }

    refrescaLista() {
        let fechaVal = this.edFecha.val;
        if (!fechaVal) return;
        this.lista.refresh();
    }

    async onLista_getRows(cb) {
        let urlOz = await zPost("getOzService.seg");
        let urlServicio = urlOz.ozConciliacion + "/getDataServer";
        let fechaVal = this.edFecha.val;
        let fecha = await new moment(fechaVal);
        let params = {
            fecha: fecha
        }

        await zPost(urlServicio, { metodo: "getPosVerifoneSP", params: params }, dataset => {
            cb(dataset);
        });
    }
}
ZVC.export(posverifone);