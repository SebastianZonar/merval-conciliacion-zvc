class Menu extends ZCustomController{
    onThis_init(){
        // this.lblNombreUsuario.view.text(window.app.nombreUsuario);
        this.lblNombreUsuario = "Patricia Lopez"; //window.app.nombreUsuario;
    }
    onOpLogout_click() {
        this.triggerEvent("logout");
    }
    onCmdPlanillaRecaudacion_click(){
        // this.triggerEvent("cmdPlanillaRecaudacion");
        this.mainLoader.load("./canales/Conciliacion")
    }

    onCmdConciliacion_click() {
        // this.triggerEvent("cmdConciliacion");
        this.mainLoader.load("./canales/Conciliacion");
    }
    
    onCmdPosBoleteria_click(){
        // this.triggerEvent("cmdPosBoleteria");
        this.mainLoader.load("./canales/Posboleteria");
    }

    onCmdAutoServicio_click(){
        // this.triggerEvent("cmdAutoServicio");
        this.mainLoader.load("./canales/Autoservicio");
    }

    onCmdPosVerifone_click(){
        // this.triggerEvent("cmdPosVerifone");
        this.mainLoader.load("./canales/Posverifone");
    }

    onCmdPosOAC_click(){
        // this.triggerEvent("cmdPosOAC");
        this.mainLoader.load("./canales/Posoac");
    }
    
    onCmdCartolasTbk_click() {
        // this.triggerEvent("cmdCartolasTbk");
        this.mainLoader.load("./cartolasTbk/movimientoTBK");
        this.lblTituloPagina.text = "Movimientos Transbank";

    }

    onCmdButton_click() {
        // this.triggerEvent("cmdButton");
        this.mainLoader.load("./carpeta1/buttons");
    }

    onCmdCard_click() {
        // this.triggerEvent("cmdCard");
        this.mainLoader.load("./carpeta1/cards");
    }

    onCmdColors_click() {
        // this.triggerEvent("cmdColors");
        this.mainLoader.load("./carpeta1/colors");
    }

    onCmdBorder_click() {
        // this.triggerEvent("cmdBorder");
        this.mainLoader.load("./carpeta1/border");
    }

    onCmdAnimations_click() {
        // this.triggerEvent("cmdAnimations");
        this.mainLoader.load("./carpeta1/animations");
    }

    onCmdOtros_click() {
        // this.triggerEvent("cmdOtros");
        this.mainLoader.load("./carpeta1/otros");
    }

    onCmdTablas_click() {
        this.triggerEvent("cmdTablas");
        this.mainLoader.load("./carpeta1/tablas");
    }

    onCmdHome_click() {
        this.triggerEvent("");
        this.mainLoader.load("./Home");
    }
    onCmdCambiarPwd_click() {
        this.showDialog("login/WCambiarPwd");
    }


    
    onVistaLoader_volver() {
        this.mainLoader.pop();
    }

    onMenutoggle_click() {
        this.menutoggle.toggleclass
    }
}

ZVC.export(Menu);