class Main extends ZCustomController {
    onThis_init() {
        $(window).resize(() => {
	        if (window.app.resize) window.app.resize();
        });
        let url = new URL(window.location.href);
        let tokenRecuperacion = url.searchParams.get("recupera");
        if (tokenRecuperacion) {
            this.mainLoader.load("login/PanelRecupera", {tokenRecuperacion:tokenRecuperacion});
        } else { 
            this.mainLoader.load("login/login");
        }
    }
    onMainLoader_login(sesion) {
        window.app.sesion = sesion;
        /*window.zSecurityToken = sesion.token;*/
       this.mainLoader.load("main/Menu");
    }
    onLoaderMain_logout() {
        this.loaderMain.load("../login/Login");
    }
    


    // //PLANILLA
    // onMenu_cmdPlanillaRecaudacion(){
    //     this.mainLoader.load("./main/planilla/planilla");
    // }
    // //CANALES
    // onMenu_cmdConciliacion(){
    //     this.mainLoader.load("./main/canales/conciliacion");
    // }
    // onMenu_cmdPosBoleteria(){
    //     this.mainLoader.load("./main/canales/posboleteria");
    // }
    // onMenu_cmdAutoServicio(){
    //     this.mainLoader.load("./main/canales/autoservicio");
    // }
    // onMenu_cmdPosVerifone(){
    //     this.mainLoader.load("./main/canales/posverifone");
    // }
    // onMenu_cmdPosOAC(){
    //     this.mainLoader.load("./main/canales/posoac");
    // }
    // //CARTOLAS TBK
    // onMenu_cmdCartolasTbk(){
    //     this.container.load("./main/cartolasTbk/movimientoTBK");
    // }
    // //CARPETA1
    // onMenu_cmdButton(){
    //     this.mainLoader.load("./main/carpeta1/buttons");
    // }
    // onMenu_cmdCard(){
    //     this.mainLoader.load("./main/carpeta1/cards");
    // }
    // onMenu_cmdColors(){
    //     this.mainLoader.load("./main/carpeta1/colors");
    // }
    // onMenu_cmdBorder(){
    //     this.mainLoader.load("./main/carpeta1/border");
    // }
    // onMenu_cmdAnimations(){
    //     this.mainLoader.load("./main/carpeta1/animations");
    // }
    // onMenu_cmdOtros(){
    //     this.mainLoader.load("./main/carpeta1/otros");
    // }
    // onMenu_cmdTablas(){
    //     this.mainLoader.load("./main/carpeta1/tablas");
    // }

};
ZVC.export(Main);