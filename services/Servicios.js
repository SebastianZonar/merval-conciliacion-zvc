const ZModule = require("../servicios/z-server").ZModule;
const SQLServer = require('./SQLServer').SQLServer;
const axios = require("axios");
const querystring = require('querystring');
let moment = require('moment-timezone');
const sqlSesion = require("../servicios/Config").getConfig().smtp;
let fs = require('fs');
var conf = require("../servicios/Config").getConfig();

class Servicios extends ZModule {
    constructor() {
        super();
    }
    static get instance() {
        if (!global.serviciosInstance) global.serviciosInstance = new Servicios();
        return global.serviciosInstance;
    }
    async createLogFile() {
        log.warn('SERVICIOS!');
    }
}
module.exports = Servicios.instance;
// exports.Servicios = Servicios;